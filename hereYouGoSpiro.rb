# => Joseph Malandruccolo
# => HW8 CSPP 51083

require 'rubygems'
require 'sinatra'
require 'aws-sdk'
require_relative 'secret'
require 'open-uri'

class Reader

# => CONSTANTS
S3_URL_PREFIX = "https://s3.amazonaws.com/cloudhw8/"
SQS_QUEUE_NAME = "myFirstQueue"
SQS_QUEUE_URL = "https://sqs.us-east-1.amazonaws.com/364316631894/myFirstQueue"


def main

	sqs = AWS::SQS.new(access_key_id: $access_key, secret_access_key: $secret_key)

	queue = sqs.queues.create(SQS_QUEUE_NAME)

	queue.poll(wait_time_seconds: 20) do |msg|
		
		msg_arr = msg.body.split('|')
		first_name = msg_arr[0]
		last_name = msg_arr[1]
		add_contact_to_s3 first_name, last_name
		msg.delete

	end
	
end



# => add contact to s3
def add_contact_to_s3 first_name, last_name

	s3 = AWS::S3.new(access_key_id: $access_key, secret_access_key: $secret_key)
	cloudhw8 = s3.buckets['cloudhw8']

	s3_key = generate_s3_key first_name, last_name
	s3_html = generate_s3_html first_name, last_name

	cloudhw8.objects.create(s3_key, s3_html)

	post_notification "#{first_name} #{last_name}", s3_html
	
	return

end


# => generate the s3 key name
def generate_s3_key first_name, last_name

	"#{first_name}#{last_name}#{Time.new.to_i}.html".downcase
	
end


# => generate the html file to store in s3
def generate_s3_html first_name, last_name

	header = "<html><head><title>Contact Page</title></head><body><table><tr><th>First Name</th><th>Last Name</th><th></th></tr>"
	data = "<tr><td>#{first_name}</td><td>#{last_name}</td><td></td></tr></table></body></html>"
	
	"#{header}#{data}"

end


def post_notification name, url_suffix

	sns = AWS::SNS.new(access_key_id: $access_key, secret_access_key: $secret_key)
	topics = sns.topics
	target_topic = nil
	topics.each do |t|
		if t.display_name.eql? "51083-updated"
			target_topic = t
		end
	end

	if !target_topic.nil?
		target_topic.publish("name: #{name}\nurl: #{S3_URL_PREFIX}#{url_suffix}", :subject => "Contact created")

		return true

	end
	
end

end


